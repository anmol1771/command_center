require 'company'
require 'item'
require 'drone'

describe 'Company' do
    before(:each) do
        @company = Company.new("ABC company")
        @drone = Drone.new(@company)
        @item = Item.new("Some address")
        @msg = { drone_id: @drone.id, item: @item.id, message: "Unloaded item #{@item.id} to #{@item.address}", time: Time.now }
    end

    describe "attributes" do
        it "allows reading and writing of :name" do
            @company.name = "DEF company"
            expect(@company.name).to eq("DEF company")
        end
        it "allows reading and writing of :messages" do
            @company.messages << @msg
            expect(@company.messages).to include(@msg)
        end
    end

    describe "sends instruction to drone to deliver item" do
        it "#deliver_item_to_its_address" do
            expect(@company.deliver_item_to_its_address(@item)).to be_truthy
            
            @item.status = "Delivered"
            expect(@company.deliver_item_to_its_address(@item)).to be_falsey

            @drone.status = "On the way"
            expect(@company.deliver_item_to_its_address(@item)).to be_falsey
        end
    end
end
