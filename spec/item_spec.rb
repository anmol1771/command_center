require 'item'

describe 'Item' do
    describe "attributes" do
        before(:each) do
            @item = Item.new("some address 1")
        end

        it "allows reading and writing of :address" do
            @item.address = "some address 2" 
            expect(@item.address).to eq("some address 2")
        end
        it "allows reading and writing of :status" do
            @item.status = "Delivered"
            expect(@item.status).to eq("Delivered")
        end
        it "allows reading of :id" do
            expect(@item.id).not_to be_nil
        end
        it "new item's default status is 'To be delivered'" do
            expect(@item.status).to eq("To be delivered")
        end
        it "shouldn't have nil address" do
            expect(@item.address).not_to be_nil
        end
    end
end