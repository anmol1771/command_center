require 'drone'
require 'company'
require 'item'

describe "Drone" do
    before(:each) do
        @company = Company.new("ABC company")
        @drone = Drone.new(@company)
        @item = Item.new("Some address")
    end

    describe "attributes" do
        it "allows reading and writing of :status" do
            @drone.status = "Busy"
            expect(@drone.status).to eq("Busy")
        end
        it "allows reading and writing of :company" do 
            @company2 = Company.new("DEF company")
            @drone.company = @company2
            expect(@drone.company).to be_a(Company)
            expect(@drone.company.name).to eq("DEF company")
        end
        it "allows reading and writing of :current_address" do
            @drone.current_address = "warehouse"
            expect(@drone.current_address).to eq("warehouse")
        end
        it "allows reading of :id" do
            expect(@drone.id).not_to be_nil
        end
        it "should have :company" do
            expect(@drone.company).to be_a(Company)
        end
        it "default status is 'Available'" do
            expect(@drone.status).to eq('Available')
        end
        it "allows reading and writing of :current_item" do
            @drone.current_item = @item
            expect(@drone.current_item).to eq(@item)
        end
    end

    describe "drone's tasks" do
        it "picks up item if item's status is 'To be delivered'" do
            expect(@drone.pick_up_item(@item)).to be_truthy
            expect(@drone.current_item).to eq(@item)
            expect(@drone.current_item.status).to eq("Picked up")

            @item.status = "Shipped"
            expect(@drone.pick_up_item(@item)).to be_falsey
        end
        it "delivers item" do
            @drone.pick_up_item(@item)
            @drone.deliver_item 
            expect(@item.status).to eq("Shipped")
        end
        it "unloads item at item's address" do 
            @drone.pick_up_item(@item)
            @drone.deliver_item
            expect(@drone.unload_item).to be_falsey

            @drone.current_address = @item.address
            expect(@drone.unload_item).to be_truthy
            expect(@drone.current_item).to be_nil
            expect(@item.status).to eq("Delivered")
        end
        it "goes back to warehouse" do
            @drone.back_to_warehouse
            expect(@drone.current_address).to eq("Warehouse")
            expect(@drone.status).to eq("Available")
        end
    end

    describe "other methods" do
        it "#send_message_to_company"
        it "#get_available_drones"
    end
end