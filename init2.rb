require_relative('lib/company')
require_relative('lib/item')
require_relative('lib/drone')

@company = Company.new("ABC company")
puts "Created company"
puts @company.inspect
puts "---------------"

def start(address)
    item = Item.new(address)
    puts "Created item"
    puts item.inspect
    puts "---------------"

    drone = Drone.new(@company)
    puts "Created drone"
    puts drone.inspect
    puts "---------------"

    puts "Sending instruction to available drone to deliver the item #{item.id}...."
    @company.deliver_item_to_its_address(item)
    puts "---------------"
end

t1 = Thread.new{start("New Delhi")}
t2 = Thread.new{start("Bangalore")}

t1.join
t2.join

puts "Drone Logs"
puts "---------------"
@company.get_drone_logs
