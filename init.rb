require_relative('lib/company')
require_relative('lib/item')
require_relative('lib/drone')

company = Company.new("ABC company")
puts "Created company"
puts company.inspect
puts "---------------"
item = Item.new("Sector 4, New Delhi")
puts "Created item"
puts item.inspect
puts "---------------"

drone = Drone.new(company)
puts "Created drone"
puts drone.inspect
puts "---------------"

puts "Sending instruction to available drone to deliver the item #{item.id}...."
company.deliver_item_to_its_address(item)
puts "---------------"

puts "Drone Logs"
puts "---------------"
company.get_drone_logs
puts "---------------"
