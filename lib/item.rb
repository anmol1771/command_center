class Item
  @@items = []
  @@item_id = 0
  attr_accessor :address, :status
  attr_reader :id
  
  def initialize(address, status = "To be delivered")
    @@item_id += 1
    @id = @@item_id
    @address = address
    @status = status
    @@items << self
  end
  
  def self.current_item_id
    @@item_id
  end
end