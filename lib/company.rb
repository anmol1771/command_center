class Company
  attr_accessor :name, :messages
  
  def initialize(name = nil)
    @name = name
    @messages = []
  end
  
  def message(msg)
    @messages << msg
  end
  
  def self.get_all_messages
    @messages
  end
  
  def get_drone_logs
    @messages.each do |a| 
      puts "#{a[:time]} - Drone id: #{a[:drone_id]} - #{a[:message]}"
    end
  end

  def deliver_item_to_its_address(item)
    begin
      drone = Drone.get_available_drones.first

      drone.pick_up_item(item)
      sleep 1
      drone.deliver_item
      sleep 5
      # at this stage, drone is on the way
      drone.current_address = item.address
      drone.unload_item
      sleep 1

      drone.back_to_warehouse
      sleep 1
      return true
    rescue
      return false
    end
  end

end