class Drone
  @@drones = []
  @@drone_id = 0
  attr_accessor :status, :company, :current_address, :current_item
  attr_reader :id
  
  def initialize(company, status = 'Available')
    @@drone_id += 1
    @id = @@drone_id
    @status = status
    @company = company
    @current_item = nil
    @current_address = "Warehouse"
    @@drones << self
  end
  
  def self.get_all_drones
    @@drones
  end
  
  def self.get_available_drones
    @@drones.select{|x| x.status == "Available"}
  end
  
  def pick_up_item(item)
    if item.status == "To be delivered"
      @current_item = item
      @current_item.status = "Picked up"
      @status = "Picked up item #{@current_item.id}"
      send_message_to_company(@current_item, @status)
      return true
    else
      return false
    end
  end
  
  def deliver_item
    @status = "Delivering item #{@current_item.id} to #{@current_item.address}"
    @current_item.status = "Shipped"
    send_message_to_company(@current_item, @status)
  end
  
  def unload_item
    if @current_address == @current_item.address
      @status = "Reached address of item #{@current_item.id}"
      send_message_to_company(@current_item, @status)
    
      @status = "Unloaded item #{@current_item.id} to #{@current_item.address}"
      @current_item.status = "Delivered"
      send_message_to_company(@current_item, @status)
      @current_item = nil
      return true
    else
      @status = "Item #{@current_item.id} address doesn't match with drone's current_address - #{@current_address}"
      send_message_to_company(@current_item, @status)
      return false
    end
  end
  
  def back_to_warehouse
    @status = "Going back to warehouse"
    send_message_to_company(nil, @status)

    sleep 5
    @current_address = "Warehouse"
    @status = "Available"
    send_message_to_company(nil, @status)
  end
  
  def send_message_to_company(item=nil, msg)
    hash = {
      drone_id: @id,
      item: item,
      message: msg,
      time: Time.now
    }
    @company.message(hash)
  end  
end